---
title: "Clean groups data"
prerequisites:
    - data/raw/data.rds
    - src/21-clean-group.R
targets:
    - data/cleaned/data.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Define paths of the project and read data

```{r}
library(ggplot2)

path_proj <- day2day::git_path()
path_raw <- file.path(path_proj, "data", "raw")
path_cleaned <- file.path(path_proj, "data", "cleaned")

source(file.path(path_proj, "src", "21-clean-group.R"))

data <- readRDS(file.path(path_raw, "data.rds"))
```

## Clean groups variable

```{r}
data <- clean_group(data)
```

## Visualize

```{r}
ggplot(data, aes(x, y)) +
    geom_point(aes(col = group))
```

## Save data

```{r}
saveRDS(data, file.path(path_cleaned, "data.rds"))
```

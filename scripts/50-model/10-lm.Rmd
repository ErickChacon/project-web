---
title: "Linear model"
prerequisites:
    - data/processed/data.rds
targets:
    - data/modelled/10-lm.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 7)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Define paths of the project and read data

```{r}
library(ggplot2)

path_proj <- day2day::git_path()
path_processed <- file.path(path_proj, "data", "processed")
path_modelled <- file.path(path_proj, "data", "modelled")
data <- readRDS(file.path(path_processed, "data.rds"))
```

## Linear model

```{r}
lm <- lm(y ~ x + group, data)
```

## Diagnostic

```{r}
par(mfrow = c(2, 2))
plot(lm)
```

## Save model

```{r}
saveRDS(lm, file.path(path_modelled, "10-lm.rds"))
```


target_all = docs/content/_index.html \
	docs/content/10-extract/_index.html \
	data/raw/data.rds \
	docs/content/10-extract/get.html \
	docs/content/20-clean/_index.html \
	data/cleaned/data.rds \
	docs/content/20-clean/clean.html \
	docs/content/30-process/_index.html \
	data/processed/data.rds \
	docs/content/30-process/process.html \
	docs/content/40-explore/_index.html \
	docs/content/40-explore/explore.html \
	docs/content/50-model/_index.html \
	data/modelled/10-lm.rds \
	docs/content/50-model/10-lm.html \
	data/modelled/10-gam.rds \
	docs/content/50-model/20-gam.html \
	docs/content/60-summarise/_index.html \
	docs/content/60-summarise/tables.html

target_clean = docs/content/_index.html \
	docs/content/10-extract/_index.html \
	docs/content/10-extract/get.html \
	docs/content/20-clean/_index.html \
	docs/content/20-clean/clean.html \
	docs/content/30-process/_index.html \
	docs/content/30-process/process.html \
	docs/content/40-explore/_index.html \
	docs/content/40-explore/explore.html \
	docs/content/50-model/_index.html \
	docs/content/50-model/10-lm.html \
	docs/content/50-model/20-gam.html \
	docs/content/60-summarise/_index.html \
	docs/content/60-summarise/tables.html

all: $(target_all)

docs/content/_index.html: \
	scripts/_index.Rmd

docs/content/10-extract/_index.html: \
	scripts/10-extract/_index.Rmd

data/raw/data.rds docs/content/10-extract/get.html: \
	scripts/10-extract/get.Rmd

docs/content/20-clean/_index.html: \
	scripts/20-clean/_index.Rmd

data/cleaned/data.rds docs/content/20-clean/clean.html: \
	scripts/20-clean/clean.Rmd \
	data/raw/data.rds \
	src/21-clean-group.R

docs/content/30-process/_index.html: \
	scripts/30-process/_index.Rmd

data/processed/data.rds docs/content/30-process/process.html: \
	scripts/30-process/process.Rmd \
	data/cleaned/data.rds

docs/content/40-explore/_index.html: \
	scripts/40-explore/_index.Rmd

docs/content/40-explore/explore.html: \
	scripts/40-explore/explore.Rmd \
	data/processed/data.rds

docs/content/50-model/_index.html: \
	scripts/50-model/_index.Rmd

data/modelled/10-lm.rds docs/content/50-model/10-lm.html: \
	scripts/50-model/10-lm.Rmd \
	data/processed/data.rds

data/modelled/10-gam.rds docs/content/50-model/20-gam.html: \
	scripts/50-model/20-gam.Rmd \
	data/processed/data.rds

docs/content/60-summarise/_index.html: \
	scripts/60-summarise/_index.Rmd

docs/content/60-summarise/tables.html: \
	scripts/60-summarise/tables.Rmd \
	data/modelled/20-gam.rds

$(target_all):
	@Rscript -e 'blogdown:::build_rmds("$(<D)/$(<F)", "docs", "scripts")'

clean:
	rm -f $(target_clean)

